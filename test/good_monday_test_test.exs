defmodule GoodMondayTestTest do
  use ExUnit.Case

  test "init function reads the file with numbers and get the greatest sum" do
    assert GoodMondayTest.init("test/fixtures/numbers") == 270
  end
end
