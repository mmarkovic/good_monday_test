defmodule GoodMondayTest do
  require Logger

  @moduledoc """
  Documentation for GoodMondayTest.
  """

  @doc """
  ## Examples

      iex> GoodMondayTest.init()
      true

  """
  def init(path) when is_binary(path) do
    numbers = path |> read_numbers |> split_numbers

    ordered_negative_numbers_sum = get_pairs(numbers, :negative_numbers, &(&1 < &2)) |> get_sum()

    ordered_positive_numbers_sum = get_pairs(numbers, :positive_numbers, &(&1 < &2)) |> get_sum()

    result = ordered_negative_numbers_sum + ordered_positive_numbers_sum
    Logger.info("Greatest possible sum is #{result}")
    result
  end

  def init(_) do
    Logger.info("Wrong path!")
    0
  end

  defp read_numbers(path) do
    case File.read(path) do
      {:ok, data} ->
        data |> parse_numbers

      {:error, :enoent} ->
        Logger.info("File not found!")
        []

      {:error, _} ->
        Logger.info("Unknown error!")
        []
    end
  end

  defp parse_numbers(data) do
    data
    |> String.split(", ")
    |> Enum.map(fn n ->
      {number, _} = Integer.parse(n)
      number
    end)
  end

  defp split_numbers(numbers) do
    numbers
    |> Enum.reduce(%{negative_numbers: [], positive_numbers: []}, fn n, acc ->
      cond do
        n <= 0 ->
          Map.put(acc, :negative_numbers, [n | acc[:negative_numbers]])

        n > 0 ->
          Map.put(acc, :positive_numbers, [n | acc[:positive_numbers]])

        true ->
          acc
      end
    end)
  end

  defp get_pairs(numbers, identifier, sorter_fn) do
    case identifier do
      :positive_numbers ->
        numbers
        |> Map.get(identifier)
        |> Enum.sort(sorter_fn)
        |> chunk_positive_numbers

      :negative_numbers ->
        numbers |> Map.get(identifier) |> Enum.sort(sorter_fn) |> Enum.chunk_every(2, 2)
    end
  end

  defp chunk_positive_numbers(numbers) do
    chunk_fun = fn element, acc ->
      cond do
        element == 1 ->
          {:cont, [element | acc], []}

        true ->
          {:cont, [element | acc]}
      end
    end

    after_fun = fn
      [] ->
        {:cont, []}

      acc ->
        {:cont, acc, []}
    end

    incomplete_chunks =
      Enum.chunk_while(
        numbers,
        [],
        chunk_fun,
        after_fun
      )

    last_element_index = length(incomplete_chunks) - 1
    chunks = incomplete_chunks |> Enum.at(last_element_index) |> Enum.chunk_every(2, 2)

    incomplete_chunks
    |> List.delete_at(last_element_index)
    |> Enum.concat(chunks)
  end

  defp get_sum(numbers) do
    numbers
    |> Enum.reduce(0, fn n, acc ->
      second_number = if Enum.at(n, 1), do: Enum.at(n, 1), else: 1
      acc + Enum.at(n, 0) * second_number
    end)
  end
end
